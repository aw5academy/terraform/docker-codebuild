resource "aws_cloudwatch_log_group" "app" {
  name              = "/aws/codebuild/docker/aw5academy/httpd"
  retention_in_days = "30"
  tags  = {
    Name = "/aws/codebuild/docker/aw5academy/httpd"
  }
}
