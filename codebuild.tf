resource "aws_codebuild_project" "app" {
  artifacts {
    type = "NO_ARTIFACTS"
  }
  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    environment_variable {
      name = "AWS_REGISTRY_HOST"
      value = "${data.aws_caller_identity.current.account_id}.dkr.ecr.${var.region}.amazonaws.com"
    }
    environment_variable {
      name = "DOCKER_PUSH"
      value = "true"
    }
    image                       = "aws/codebuild/amazonlinux2-x86_64-standard:2.0"
    privileged_mode             = "true"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"
  }
  logs_config {
    cloudwatch_logs {
      group_name = aws_cloudwatch_log_group.app.name
    }
  }
  name          = "docker-aw5academy-httpd"
  service_role  = aws_iam_role.codebuild.arn
  source {
    type     = "CODECOMMIT"
    location = aws_codecommit_repository.app.clone_url_http
  }
  tags = {
    Name = "docker-aw5academy-httpd"
  }
}
